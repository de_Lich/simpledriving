using System;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public const string HighScoreKey = "HighScore";
    [SerializeField] TMP_Text scoreText = null;
    [SerializeField] TMP_Text speedText = null;
    [SerializeField] TMP_Text startCountText = null;

    [SerializeField] private float startCount;
    [SerializeField] private float middleSpeed = 0;
    [SerializeField] private float highSpeed = 0;

    private float score;
    private bool isStarted;

    public static event Action <bool> StartMovement;
    private void Awake()
    {
        //StartMovement += StartPointCount;
        CarController.SpeedIncreasing += ShowSpeed; 
    }

    public void StartPointCount(bool isStarted)
    {
        this.isStarted = isStarted;
    }

    void Update()
    {
        StartTimer();
        if(isStarted)
            ScoreCount();
    }

    private void StartTimer()
    {
        startCount -= Time.deltaTime;
        startCountText.text = Mathf.FloorToInt(startCount).ToString();
        if(startCount < 1)
        {
            startCountText.text = "GO";
        }
        if(startCount < 0)
        {
            StartMovement?.Invoke(true);
        }
        if(startCount < -1)
        {

            startCountText.gameObject.SetActive(false);
        }
    }

    private void ScoreCount()
    {
        score += Time.deltaTime;
        scoreText.text = "Points: " + Mathf.FloorToInt(score).ToString();
    }

    private void ShowSpeed(float speed)
    {
        speedText.text = Mathf.FloorToInt(speed).ToString() + " km/h";
        if (speed > middleSpeed)
        {
            speedText.color = Color.yellow;
        }
        if (speed > highSpeed)
        {
            speedText.color = Color.red;
        }
        else { speedText.color = Color.green; }
    }
    private void OnDisable()
    {
        CarController.SpeedIncreasing -= ShowSpeed;
    }
    private void OnDestroy()
    {
        int  currentHighScore = PlayerPrefs.GetInt(HighScoreKey, 0);

        if(score > currentHighScore)
        {
            PlayerPrefs.SetInt(HighScoreKey, Mathf.FloorToInt(score));
        }
    }
}
