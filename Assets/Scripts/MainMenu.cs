using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] TMP_Text highScoretxt;
    [SerializeField] TMP_Text energytxt;
    [SerializeField] Button playButton;
    [SerializeField] private int maxEnergy;
    [SerializeField] private int energyRechargeDuration;
    [SerializeField] AndroidNotificationHandler androidNotificationHandler;
    //[SerializeField] IOSNotificationHandler iOSNotificationHandler;

    private int energy;
    private const string EnergyKey = "Energy";
    private const string EnergyReadyKey = "EnergyReady";

    private void Start()
    {
        OnApplicationFocus(true);
    }
    private void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus) { return; }

        CancelInvoke();
        ShowScore();

        energy = PlayerPrefs.GetInt(EnergyKey, maxEnergy);
        if(energy == 0)
        {
            string energyReadyString = PlayerPrefs.GetString(EnergyReadyKey, string.Empty);
            if(energyReadyString == string.Empty) { return; }

            DateTime energyReady = DateTime.Parse(energyReadyString);

            if(DateTime.Now > energyReady)
            {
                energy = maxEnergy;
                PlayerPrefs.SetInt(EnergyKey, energy);
            }
            else
            {
                playButton.interactable = false;
                Invoke(nameof(EnergyRecharged), (energyReady - DateTime.Now).Seconds);
            }
        }
        energytxt.text = $"Play ({energy})";
    }

    private void EnergyRecharged()
    {
        playButton.interactable = true;
        energy = maxEnergy;
        PlayerPrefs.SetInt(EnergyKey, energy);
        energytxt.text = $"Play ({energy})";
    }
    private void ShowScore()
    {
        int highScore = PlayerPrefs.GetInt(UIManager.HighScoreKey, 0);
        highScoretxt.text = "Highscore: " + highScore.ToString();
    }

    public void PlayGame()
    {
        if(energy < 1) { return; }
        energy--;
        PlayerPrefs.SetInt(EnergyKey, energy);
        if(energy == 0)
        {
            DateTime energyReady = DateTime.Now.AddMinutes(energyRechargeDuration);
            PlayerPrefs.SetString(EnergyReadyKey, energyReady.ToString());
#if UNITY_ANDROID
            androidNotificationHandler.ScheduleNotification(energyReady);
            //#if UNITY_IOS
            //iOSNotificationHandler.ScheduleNotification(energyRechargeDuration);
#endif
        }
        SceneManager.LoadScene(1);
    }
    public void ExitApplication()
    {
        Application.Quit();
    }
    public void ClearScore()
    {
        PlayerPrefs.DeleteKey(UIManager.HighScoreKey);
        ShowScore();
    }
}
