using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class CarBrakesSpawner : MonoBehaviour
{
    [SerializeField] Transform [] spawnPoints = null;
    [SerializeField] GameObject carbrakePrefab = null;
    [SerializeField] private int firstAppearTime;
    [SerializeField] private float timeInterval;
    [SerializeField] private bool brakeSpawned;
    [SerializeField] int Randomcounter;
    void Start()
    {
        brakeSpawned = true;
        Invoke(nameof(TurnOnBrakes), firstAppearTime);
        StartCoroutine(SpawnBrakes());
    }

    public void TurnOnBrakes() 
    {
        brakeSpawned = true;
    }

    private IEnumerator SpawnBrakes()
    {
        while (brakeSpawned)
        {
            Randomcounter = Random.Range(0, spawnPoints.Length);
            Vector3 position = spawnPoints[Randomcounter].position;
            GameObject brakeInstance = Instantiate(carbrakePrefab, position, Quaternion.identity);
            brakeSpawned = false;
            yield return new WaitForSeconds(timeInterval);
        }
    }
}
