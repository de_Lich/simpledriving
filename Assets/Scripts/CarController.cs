using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class CarController : MonoBehaviour
{
    [SerializeField] float timer;
    [Header("Car settings")]
    [SerializeField] float speed = 0f;
    [SerializeField] float speedGainPerSecond = 0f;
    [SerializeField] float turnSpeed = 0f;

    [Header ("Accedent settings")]
    [SerializeField] Vector3 accedentForceVector;
    [SerializeField] Vector3 accedentCarRotationVector;
    [SerializeField] float lvlEndDelay;

    [SerializeField] private bool isAlive = true;
    private Rigidbody rigidbody;


    public static event Action <float> SpeedIncreasing;
    private int steerValue;

    private void Awake()
    {
        UIManager.StartMovement += IsAlive;
    }
    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        IsAlive(false);
    }
    void Update()
    {
        if (isAlive)
        {
            SpeedIncrease();
            RotateCar();
            MoveCar();
        }
    }
    public void IsAlive(bool isAlive)
    {
        this.isAlive = isAlive;
    }
    private void SpeedIncrease()
    {
        speed += speedGainPerSecond * Time.deltaTime;
        SpeedIncreasing?.Invoke(speed);
        if (Touchscreen.current.primaryTouch.press.isPressed)
        {
            Vector2 touchPosition = Touchscreen.current.primaryTouch.position.ReadValue();
            Steer((int)Mathf.Sign(touchPosition.x - (Screen.width / 2.0f)));
        }
    }

    private void MoveCar()
    {
        transform.Translate(Vector3.forward * speed * speedGainPerSecond * Time.deltaTime);
    }

    private void RotateCar()
    {
        transform.Rotate(0f, steerValue * turnSpeed * Time.deltaTime, 0f);
    }

    public void Steer(int value)
    {
        steerValue = value;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))
        {
            EndGame();
        }
        if (other.CompareTag("Barrel"))
        {
            DecreaseSpeed(10f);
            //brakeTrailFX.Play();
        }
    }
    public void DecreaseSpeed(float decreaseValue)
    {
        speed -= decreaseValue;
    }
    private void EndGame()
    {
        if (isAlive)
        {
            PreporationToExplode();
        }
        Invoke(nameof(CallMenuScene), lvlEndDelay);
    }

    private void PreporationToExplode()
    {
        accedentForceVector.y += speed;
        accedentForceVector.z += speed;
        accedentCarRotationVector.x += speed;
        //UIManager.in
        UIManager.StartMovement -= IsAlive;
        IsAlive(false);
        GetComponent<BoxCollider>().isTrigger = false;
        rigidbody.useGravity = enabled;
        rigidbody.isKinematic = false; 
        rigidbody.AddForce(accedentForceVector, ForceMode.Impulse);
        rigidbody.AddRelativeTorque(accedentCarRotationVector, ForceMode.Impulse);
    }

    private void CallMenuScene()
    {
        SceneManager.LoadScene("Menu");
    }
    private void OnDisable()
    {
        UIManager.StartMovement -= IsAlive;
    }
}
