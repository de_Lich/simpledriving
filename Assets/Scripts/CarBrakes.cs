using UnityEngine;

public class CarBrakes : MonoBehaviour
{
    [SerializeField] private float value;
    [SerializeField] private ParticleSystem destroyFX;
    private CarBrakesSpawner carBrakeSpawner;
    private void Start()
    {
        carBrakeSpawner = FindObjectOfType<CarBrakesSpawner>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other == null) { return; }

        //destroyFX.Play();
        carBrakeSpawner.TurnOnBrakes();
        Destroy(gameObject);

    }
}
