using System.Collections.Generic;
using UnityEngine;

public class WheelRotate : MonoBehaviour
{
    [SerializeField] List<Transform> wheels = null;
    [SerializeField] float rotationSpeed = 200f;

    void Update()
    {
        RotateWheel(wheels);
        //TurnWheel(wheels);
    }
    private void RotateWheel(List<Transform> wheels)
    {
        foreach (Transform wheel in wheels)
        {
            wheel.Rotate(rotationSpeed * Time.deltaTime, 0f, 0f);
        }
    }
    private void TurnWheel(List<Transform> wheels)
    {
        if (Input.GetKey(KeyCode.A))
        {
            foreach (Transform wheel in wheels)
            {
                wheel.Rotate(0f, -50 * Time.deltaTime, 0f);
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            foreach (Transform wheel in wheels)
            {
                wheel.Rotate(0f, 50 * Time.deltaTime, 0f);
            }
        }
    }
}
